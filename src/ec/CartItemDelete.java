package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CartBeans;

@WebServlet("/CartItemDelete")
public class CartItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CartItemDelete() {
        super();
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		String[] check = request.getParameterValues("item_check");
		ArrayList<CartBeans> cartItem = (ArrayList<CartBeans>) session.getAttribute("cartItem");

		if (check != null) {
			//削除対象の商品を削除
			for (String deleteItemId : check) {
				for (CartBeans cart: cartItem) {
					if (cart.getId() == Integer.parseInt(deleteItemId)) {
						cartItem.remove(cart);
						break;
					}
				}
			}
		}

		response.sendRedirect("ProductListServlet");
	}

}
