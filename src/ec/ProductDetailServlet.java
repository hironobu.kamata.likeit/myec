package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import beans.ReviewsBeans;
import dao.MasterDao;
import dao.UserDao;

@WebServlet("/ProductDetailServlet")
public class ProductDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public ProductDetailServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		try {
			int id = Integer.parseInt(request.getParameter("id"));
			MasterDao master = new MasterDao();
			ItemBeans itemDate = master.findItem(id);
			request.setAttribute("itemDate", itemDate);

//  -----------------------------------------------------------------------
			UserDao userDao = new UserDao();
			List<ReviewsBeans> review = userDao.reviewsList(id);

			if(review.isEmpty()) {
				String err = "この商品にはまだレビューがありません。";
				request.setAttribute("err", err);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/product_detail.jsp");
				dispatcher.forward(request, response);
			}

			request.setAttribute("reviewList", review);

//  -----------------------------------------------------------------------
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/product_detail.jsp");
			dispatcher.forward(request, response);

		} catch(Exception e){
			e.printStackTrace();
			response.sendRedirect("IndexServlet");
		}
	}

}
