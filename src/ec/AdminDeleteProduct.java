package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.MasterDao;

@WebServlet("/AdminDeleteProduct")
public class AdminDeleteProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminDeleteProduct() {
    	super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("AdminLoginServlet");
			return;
		}
//	------------------------------------------------------------------------

		int id = Integer.parseInt(request.getParameter("id"));

//		リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		MasterDao masterDao = new MasterDao();
		ItemBeans itemDate = masterDao.findItem(id);

		request.setAttribute("itemDate", itemDate);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/admin_delete_product.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int item_id = Integer.parseInt(request.getParameter("id"));

		MasterDao masterDao = new MasterDao();
		masterDao.deleteItem(item_id);

		response.sendRedirect("AdminTopServlet");
	}

}
