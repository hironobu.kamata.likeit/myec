package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.MasterDao;

@WebServlet("/AdminDetailServlet")
public class AdminDetailProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminDetailProductServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("AdminLoginServlet");
			return;
		}
//	------------------------------------------------------------------------

		int id = Integer.parseInt(request.getParameter("id"));

//		リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		MasterDao masterDao = new MasterDao();
		ItemBeans itemDate = masterDao.findItem(id);

		HttpSession session2 = request.getSession();
		session2.setAttribute("itemDate", itemDate);


		if(itemDate.getItem_count() <= 10 && itemDate.getItem_count() >= 1) {
			request.setAttribute("errMsg", "残りの在庫が少なくなっています。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/admin_detail_product.jsp");
			dispatcher.forward(request, response);
		}else if (itemDate.getItem_count() <= 0) {
			request.setAttribute("errMsg", "在庫がありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/admin_detail_product.jsp");
			dispatcher.forward(request, response);
		}

		ItemBeans categoryName = masterDao.categorySearch(itemDate.getCategory_id());
		HttpSession session3 = request.getSession();
		session3.setAttribute("categoryName", categoryName);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/admin_detail_product.jsp");
		dispatcher.forward(request, response);
	}

}
