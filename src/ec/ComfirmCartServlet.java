package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CartBeans;
import beans.UserBeans;

@WebServlet("/ComfirmCartServlet")
public class ComfirmCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ComfirmCartServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("EcLoginServlet");
			return;
		}
//	    ---------------------------------------------------
		HttpSession session1 = request.getSession();
		ArrayList<CartBeans> cartItem = (ArrayList<CartBeans>) session1.getAttribute("cartItem");

//		セッションにcartItemがない場合作成  --------------------
		if (cartItem == null) {
			cartItem = new ArrayList<CartBeans>();
			session.setAttribute("cartItem", cartItem);
		}
//		----------------------------------------------------
//		カートに商品がない場合はメッセージ
		String cartMessage = "";

		if(cartItem.size() == 0) {
			cartMessage = "カートに商品が入っていません。";
		}

		request.setAttribute("cartMessage", cartMessage);

//		---------------------------------------------------
//		jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/comfirm_cart.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		ArrayList<CartBeans> cartItem = (ArrayList<CartBeans>) session.getAttribute("cartItem");

//		セッションにcartItemがない場合作成  --------------------
		if (cartItem == null) {
			cartItem = new ArrayList<CartBeans>();
			session.setAttribute("cartItem", cartItem);
		}
//		---------------------------------------------------
//		商品情報の取得
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		int price = Integer.parseInt(request.getParameter("price"));
		int item_count = Integer.parseInt(request.getParameter("item_count"));

//		個数　* 単価
		int countPrice = price * item_count;
//		合計金額
		int totalPrice = 0;

		CartBeans cb = new CartBeans(id, name, item_count, price, countPrice, totalPrice);

		cartItem.add(cb);

		session.setAttribute("cartItem", cartItem);
//		------------------------------------------------------
//		jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/comfirm_cart.jsp");
		dispatcher.forward(request, response);

	}

}
