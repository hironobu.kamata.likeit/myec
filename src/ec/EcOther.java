package ec;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import beans.CartBeans;
import beans.ItemBeans;

public class EcOther {


	public static int getTotalPrice(ArrayList<CartBeans> cartItem) {
		int total = 0;
		for (CartBeans item : cartItem) {
			total += item.getCountPrice();
		}
		return total;
	}


	public static int salePrice(List<ItemBeans> item2) {
		double total = 0;
		for (ItemBeans item : item2) {
		 	total = item.getItem_price() - (item.getItem_price() * 0.3);
		}
		return (int)total;
	}


	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}


}









