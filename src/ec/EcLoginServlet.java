package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

@WebServlet("/EcLoginServlet")
public class EcLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public EcLoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ec_login.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

	    // リクエストパラメータの入力項目を取得
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");

		UserDao userDao = new UserDao();
		UserBeans user = userDao.findByLoginInfo(login_id, password);

		if(user == null) {
			request.setAttribute("errMsg", "ログインに失敗しました。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ec_login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// セッションにユーザの情報をセット
		HttpSession session2 = request.getSession();
		session2.setAttribute("userInfo", user);
		session2.setAttribute("userName", user.getUser_name());

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("ProductListServlet");

	}

}



