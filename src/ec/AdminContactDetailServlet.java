package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ContactBeans;
import beans.UserBeans;
import dao.MasterDao;

@WebServlet("/AdminContactDetailServlet")
public class AdminContactDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminContactDetailServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
//		ログイン情報ない場合 --------------------------------------------------
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("AdminLoginServlet");
			return;
		}
//	------------------------------------------------------------------------
		int id = Integer.parseInt(request.getParameter("id"));

		System.out.println(id);

		MasterDao masterDao = new MasterDao();
	    ContactBeans contactDate = masterDao.contact(id);

		request.setAttribute("con", contactDate);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/admin-contact-detail.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int id = Integer.parseInt(request.getParameter("id"));
		MasterDao.deleteContact(id);

		response.sendRedirect("AdminContactServlet");
	}

}







