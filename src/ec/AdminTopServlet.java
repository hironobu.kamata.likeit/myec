package ec;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HistoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.MasterDao;
import dao.UserDao;

@WebServlet("/AdminTopServlet")
public class AdminTopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminTopServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//	------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("AdminLoginServlet");
			return;
		}
//	------------------------------------------------------------------------
//		商品情報
		MasterDao masterDao = new MasterDao();
		List<ItemBeans> item = masterDao.itemAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("itemList", item);

//		------------------------------------------------------------------------
//		今月の売り上げ

		Date date = new Date();

		UserDao month = new UserDao();
		HistoryBeans monthPrice = month.monthSale(date);

		request.setAttribute("monthPrice", monthPrice);

//		------------------------------------------------------------------------

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/admin_top.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
