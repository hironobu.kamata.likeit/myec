package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ContactBeans;
import beans.UserBeans;
import dao.MasterDao;

@WebServlet("/AdminContactServlet")
public class AdminContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminContactServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("AdminLoginServlet");
			return;
		}
//	------------------------------------------------------------------------

		MasterDao master = new MasterDao();
		List<ContactBeans> contact = master.contactList();

		String message = "";
		if(contact.isEmpty()) {
			message = "問合せはありません。";
		}
		request.setAttribute("message", message);

		request.setAttribute("contact", contact);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/admin-contact.jsp");
		dispatcher.forward(request, response);
	}

}









