package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HistoryBeans;
import beans.ItemBeans;
import beans.ReviewsBeans;
import beans.UserBeans;
import dao.UserDao;

@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("EcLoginServlet");
			return;
		}
//	------------------------------------------------------------------------
//		購入履歴
		UserDao userDao = new UserDao();
		List<HistoryBeans> history = userDao.findHistory(userInfo.getLogin_id());

		request.setAttribute("history_list", history);
//	------------------------------------------------------------------------
// 		欲しいものリスト
		UserDao userDao2 = new UserDao();
		List<ItemBeans> wishItemDate = userDao2.wishItemSeach(userInfo.getLogin_id());

		String err1 = "";
		if(wishItemDate.isEmpty()) {
			err1 = "欲しいものリストは空です。";
		}
		request.setAttribute("err1", err1);

		request.setAttribute("wishItemDate", wishItemDate);

//		------------------------------------------------------------------------
// 		投稿したレビュー
		UserDao userDao3 = new UserDao();
		List<ReviewsBeans> userReview = userDao3.userReviewsList(userInfo.getLogin_id());

		if(userReview.isEmpty()) {
			String err2 = "まだレビューを投稿していません。";
			request.setAttribute("err2", err2);
		}

		request.setAttribute("userReview", userReview);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String login_id = request.getParameter("login_id");
		String user_name = request.getParameter("user_name");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");

		if(!(password.equals(password2))) {
			request.setAttribute("errMsg", "パスワードが違います。");
			//	jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
			dispatcher.forward(request, response);
			return;
		}

//		リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
	    UserDao userDao = new UserDao();
		userDao.updateUser(user_name, password, login_id);

		HttpSession session = request.getSession();
		session.setAttribute("userName", user_name);

		response.sendRedirect("UserServlet");
	}

}
