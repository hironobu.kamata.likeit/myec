package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import dao.MasterDao;
import dao.UserDao;

@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public IndexServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		アイテム一覧
		MasterDao masterDao = new MasterDao();
		List<ItemBeans> item = masterDao.itemAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("itemList", item);

//		------------------------------------------------------------------------------
//		新着情報
		UserDao userDao = new UserDao();
		List<ItemBeans> item2 = userDao.newItemList();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("itemDate", item2);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
