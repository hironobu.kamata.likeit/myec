package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.MasterDao;

@WebServlet("/AdminUpdateServlet")
public class AdminUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminUpdateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("AdminLoginServlet");
			return;
		}
//	------------------------------------------------------------------------

		int id = Integer.parseInt(request.getParameter("id"));

//		リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		MasterDao masterDao = new MasterDao();
		ItemBeans itemDate = masterDao.findItem(id);

		request.setAttribute("itemDate", itemDate);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/admin_update_product.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
        int item_id = Integer.parseInt(request.getParameter("item_id"));
		String item_name  = request.getParameter("item_name");
		String item_text = request.getParameter("item_text");
		int item_price  = Integer.parseInt(request.getParameter("item_price"));
		int item_count = Integer.parseInt(request.getParameter("item_count"));
		String file_name = request.getParameter("file_name");
		int category_id = Integer.parseInt(request.getParameter("category_id"));

		//----------------------------------------------------
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		MasterDao masterDao = new MasterDao();
		masterDao.updateItem(item_name, item_text, item_price, item_count, file_name, category_id, item_id);

		response.sendRedirect("AdminTopServlet");
	}

}
