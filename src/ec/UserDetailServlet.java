package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.HistoryBeans;
import beans.HistoryDetailBeans;
import beans.UserBeans;
import dao.UserDao;


@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public UserDetailServlet() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("EcLoginServlet");
			return;
		}
//	------------------------------------------------------------------------

		int historyId = Integer.parseInt(request.getParameter("historyId"));
		request.setAttribute("HistoryId", historyId);

		UserDao userDao = new UserDao();
	    HistoryBeans history = userDao.findHistorydetail1(historyId);

		request.setAttribute("historyDate", history);

//		------------------------------------------------------------------------------------------
		UserDao userDao2 = new UserDao();
		List<HistoryDetailBeans> history2 = userDao2.findHistoryDetail(historyId);

		request.setAttribute("historyDetailDate", history2);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_detail.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
