package ec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MasterDao;

@WebServlet("/AdminSaleDeleteServlet")
public class AdminSaleDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminSaleDeleteServlet() {
        super();
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String[] check = request.getParameterValues("item_check");
		int item_id = Integer.parseInt(request.getParameter("item_id"));

		if (check != null) {
			MasterDao masterDao = new MasterDao();
			masterDao.deleteSaleItem(item_id);
		}



		response.sendRedirect("AdminSaleServlet");
	}

}
