package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.MasterDao;

@WebServlet("/AdminSaleServlet")
public class AdminSaleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminSaleServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("AdminLoginServlet");
			return;
		}
//	------------------------------------------------------------------------
//		セールアイテム表示
		MasterDao masterDao = new MasterDao();
		List<ItemBeans> item = masterDao.saleItemAll();

		request.setAttribute("itemList", item);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/admin_sale_item.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

//      セールアイテムの追加
		String item_name  = request.getParameter("item_name");
		int item_id = Integer.parseInt(request.getParameter("item_id"));

		MasterDao masterDao = new MasterDao();
		masterDao.newSaleItem(item_id, item_name);

		response.sendRedirect("AdminSaleServlet");
	}

}











