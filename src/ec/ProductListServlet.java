package ec;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.MasterDao;
import dao.UserDao;

@WebServlet("/ProductListServlet")
public class ProductListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ProductListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("EcLoginServlet");
			return;
		}
//	------------------------------------------------------------------------
			// アイテム情報
			MasterDao masterDao = new MasterDao();
			List<ItemBeans> item = masterDao.itemAll();

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("itemSerch", item);
//	------------------------------------------------------------------------
			// 人気の商品　取得
			UserDao userDao = new UserDao();
			List<ItemBeans> popularItem = userDao.popularItemList();

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("popularItem", popularItem);
//	------------------------------------------------------------------------
//			セールアイテム表示
			UserDao masterDao2 = new UserDao();
			List<ItemBeans> item2 = masterDao2.saleItemAll();

//			 現在時刻
			 Date date = new Date();

//			 セール開始時刻
			 Calendar cl = Calendar.getInstance();
			 cl.set(Calendar.HOUR_OF_DAY, 6);
			 cl.set(Calendar.MINUTE, 0);

//			 セール終了時刻
			 Calendar cl2 = Calendar.getInstance();
			 cl2.set(Calendar.HOUR_OF_DAY, 18);
			 cl2.set(Calendar.MINUTE, 0);

//			 calender型からdate型に変換
			 Date date1 = cl.getTime();
			 Date date2 = cl2.getTime();

//			 現在時刻と比較　範囲内の場合は表示
			 if(date.after(date1) && date.before(date2)) {
				 request.setAttribute("itemSaleList", item2);
			 }

//	------------------------------------------------------------------------
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/product_list.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
//		検索ワード
		String seach_name = request.getParameter("serch_name");
//		カテゴリー
		int category_id = Integer.parseInt(request.getParameter("select_serch"));

		UserDao userDao = new UserDao();
		List<ItemBeans> item = userDao.find(seach_name, category_id);

		String message = "";
		if(item.size() == 0) {
			message = "該当する商品がありません";
			request.setAttribute("message", message);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/product_list.jsp");
			dispatcher.forward(request, response);
		}

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("itemList", item);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/product_list.jsp");
		dispatcher.forward(request, response);
	}

}
