package ec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

@WebServlet("/WishServlet")
public class WishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public WishServlet() {
        super();
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		int item_id = Integer.parseInt(request.getParameter("id"));

		UserDao userDao = new UserDao();
		userDao.wishItem(userInfo.getLogin_id(), item_id);

		response.sendRedirect("UserServlet");
	}

}
