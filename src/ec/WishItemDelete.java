package ec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;


@WebServlet("/WishItemDelete")
public class WishItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public WishItemDelete() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		int id = Integer.parseInt(request.getParameter("wish_id"));

//		リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
	    UserDao userDao = new UserDao();
		userDao.deleteItem(id);

		response.sendRedirect("UserServlet");
	}

}
