package ec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CartBeans;
import beans.UserBeans;
import dao.UserDao;

@WebServlet("/BuyComfirmServlet")
public class BuyComfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public BuyComfirmServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session2 = request.getSession();
		UserBeans userInfo = (UserBeans) session2.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("EcLoginServlet");
			return;
		}
//	------------------------------------------------------------------------
		HttpSession session = request.getSession();
		ArrayList<CartBeans> cartItem = (ArrayList<CartBeans>) session.getAttribute("cartItem");

//		カートに商品がない場合はカート画面に遷移
		String cartMessage = "";

		if(cartItem.size() == 0) {
			cartMessage = "カートに商品が入っていません。";
			request.setAttribute("cartMessage", cartMessage);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/comfirm_cart.jsp");
			dispatcher.forward(request, response);

			return;
		}

		int totalPrice = EcOther.getTotalPrice(cartItem);
//		合計金額
		session.setAttribute("totalPrice", totalPrice);
//	------------------------------------------------------------------------------------------------------

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy_comfirm.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

//		------------------------------------------------------------------------------------------------------
//		購入履歴登録　
		String login_id = request.getParameter("login_id");
		int totalPrice = Integer.parseInt(request.getParameter("total_price"));

		int historyId = 0;
		try {
			historyId = UserDao.newHistory(login_id, totalPrice);
		}
		catch (SQLException e) {
			e.printStackTrace();
		}

//		------------------------------------------------------------------------------------------------------
//		購入履歴登録　詳細
		ArrayList<CartBeans> cartItem = (ArrayList<CartBeans>) session.getAttribute("cartItem");

		for(CartBeans item : cartItem) {
			UserDao userDao = new UserDao();
			userDao.newHistoryDetail(historyId, item.getId(), item.getCount());
		}
//		------------------------------------------------------------------------------------------------------
//　　　 在庫
		for(CartBeans item : cartItem) {
			UserDao userDao = new UserDao();
			userDao.stock(item.getId(), item.getCount());
		}

//		------------------------------------------------------------------------------------------------------
//		カート情報を破棄
		session.removeAttribute("cartItem");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/completed.jsp");
		dispatcher.forward(request, response);
	}
}

