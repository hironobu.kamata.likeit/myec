package ec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

@WebServlet("/NewReviewServlet")
public class NewReviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public NewReviewServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int item_id = Integer.parseInt(request.getParameter("item_id"));
		String review_title = request.getParameter("review-title");
		String review_text = request.getParameter("review-text");
		String user_name = request.getParameter("user_name");
		String login_id = request.getParameter("login_id");


		UserDao userDao = new UserDao();
		userDao.newReviews(item_id, review_title, review_text, user_name, login_id);

		response.sendRedirect("ProductListServlet");
	}

}
