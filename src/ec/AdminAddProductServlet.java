package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.MasterDao;

@WebServlet("/AdminAddProductServlet")
public class AdminAddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AdminAddProductServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		------------------------------------------------------------------------
//		ログイン情報ない場合
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("AdminLoginServlet");
			return;
		}
//	------------------------------------------------------------------------

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/add_product.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String item_name  = request.getParameter("item_name");
		String item_text = request.getParameter("item_text");
		int item_price  = Integer.parseInt(request.getParameter("item_price"));
		int item_count = Integer.parseInt(request.getParameter("item_count"));
		String file_name = request.getParameter("file_name");
		int category_id = Integer.parseInt(request.getParameter("category_id"));

		//----------------------------------------------------
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		MasterDao masterDao = new MasterDao();
		masterDao.newItem(item_name, item_text, item_price, item_count, file_name, category_id);

		response.sendRedirect("AdminTopServlet");
	}

}
