package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import beans.HistoryBeans;
import beans.HistoryDetailBeans;
import beans.ItemBeans;
import beans.ReviewsBeans;
import beans.UserBeans;
import db.DBManager;

public class UserDao {

	public static int getTotalItemPrice(ArrayList<ItemBeans> items) {
		int total = 0;
		for (ItemBeans item : items) {
			total += item.getItem_price();
		}
		return total;
	}

//　ログイン情報　------------------
	public UserBeans findByLoginInfo(String login_id, String password) {
        Connection conn = null;

      password = PassMD(password); // ハッシュ化

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

            // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, login_id);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }
        	// 必要なデータのみインスタンスのフィールドに追加
            String nameData = rs.getString("user_name");
            String loginIdData = rs.getString("login_id");

            return new UserBeans(nameData, loginIdData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

// --------------------------------------------------------------------------------
//  新規登録 ------------------------------------------------------------------------
	public void newUser(String user_name, String login_id, String password) {
		Connection conn = null;
		PreparedStatement stmt = null;

		password = PassMD(password); // ハッシュ化

	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();
	        // SELECT文を準備
	        String sql = "INSERT INTO user(user_name, login_id, password, create_date, update_date)"
	        		   + "VALUES(?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);";

	        // SELECTを実行し、結果表を取得
	        stmt = conn.prepareStatement(sql);
	        stmt.setString(1, user_name);
	        stmt.setString(2, login_id);
	        stmt.setString(3, password);

	        stmt.executeUpdate();
	    	stmt.close();

	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	    	try {
	        // データベース切断
	        if (conn != null) {
	            conn.close();
	        }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
		}
	}
//  ------------------------------------------------------------------------------------
//	アップデート 更新 ユーザー
	public void updateUser(String user_name, String password, String login_id) {
		Connection conn = null;
		PreparedStatement stmt = null;

		password = PassMD(password); // ハッシュ化

	    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        String sql = "UPDATE user SET user_name = ?, password = ?, update_date = CURRENT_TIMESTAMP WHERE login_id = ?;";

        stmt = conn.prepareStatement(sql);
        stmt.setString(1, user_name);
        stmt.setString(2, password);
        stmt.setString(3, login_id);

        stmt.executeUpdate();
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}

//  ------------------------------------------------------------------------------------
// ユーザー情報削除
	public void deleteUser(String login_id) {

		Connection conn = null;
		PreparedStatement stmt = null;

	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();
	        // SELECT文を準備
	        String sql = "DELETE FROM user WHERE login_id = ?;";

	        // SELECTを実行し、結果表を取得
	        stmt = conn.prepareStatement(sql);
	        stmt.setString(1, login_id);

	        stmt.executeUpdate();
	    	stmt.close();

	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	    	try {
	        // データベース切断
	        if (conn != null) {
	            conn.close();
	        }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
		}
	}

//　検索　------------------------------------
	  public List<ItemBeans> find(String serch_name, int category_id) {
	        Connection conn = null;

	        List<ItemBeans> itemList = new ArrayList<ItemBeans>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
	            String sql = " SELECT * FROM item WHERE item_name LIKE '%"+serch_name+"%'";

	            if(!(category_id == 0)) {
	            	sql += " AND category_id = '" + category_id + "'";
	            }

	            // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int item_id = rs.getInt("item_id");
	                String item_name = rs.getString("item_name");
	                String item_text = rs.getString("item_text");
	                int item_price = rs.getInt("item_price");
	                int item_count = rs.getInt("item_count");
	                String file_name = rs.getString("file_name");
	                ItemBeans item = new ItemBeans(item_id, item_name, item_text, item_price, item_count, file_name);

	                itemList.add(item);
	            }

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return itemList;
	    }

//　ハッシュ化　------------------------------------

public String PassMD(String pass_md) {

	//ハッシュを生成したい元の文字列
	String source = pass_md;
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = null;

	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
	}

	String result = DatatypeConverter.printHexBinary(bytes);

	return result;

}

// ----------------------------------------------------------------------------------------------------
//　　購入履歴　登録
public static int newHistory(String login_id, int total_price) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	int autoIncKey = -1;
	try {
		con = DBManager.getConnection();
		st = con.prepareStatement(
				"INSERT INTO history(login_id, price_total, buy_day) VALUES(?,?,CURRENT_TIMESTAMP)",Statement.RETURN_GENERATED_KEYS);
		st.setString(1, login_id);
		st.setInt(2, total_price);
		st.executeUpdate();

		ResultSet rs = st.getGeneratedKeys();
		if (rs.next()) {
			autoIncKey = rs.getInt(1);
		}

		return autoIncKey;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
}

//購入履歴 詳細 登録
public void newHistoryDetail(int history_id, int item_id, int item_count) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
		 // データベースへ接続
		 conn = DBManager.getConnection();
		 // SELECT文を準備
		 String sql = "INSERT INTO history_detail(history_id, item_id, item_count) VALUES(?, ?, ?);";

		 // SELECTを実行し、結果表を取得
		 stmt = conn.prepareStatement(sql);
		 stmt.setInt(1, history_id);
		 stmt.setInt(2, item_id);
		 stmt.setInt(3, item_count);

		 stmt.executeUpdate();
			 stmt.close();

		} catch (SQLException e) {
		 e.printStackTrace();
		} finally {
			try {
		 // データベース切断
		 if (conn != null) {
		     conn.close();
		 }
		 } catch (SQLException e) {
		     e.printStackTrace();
		 }
	 }
 }

//----------------------------------------------------------------------------------------------------
//購入履歴　表示
public List<HistoryBeans> findHistory(String loginId) {
    Connection conn = null;

    List<HistoryBeans> historyList = new ArrayList<HistoryBeans>();

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM history WHERE login_id = '" + loginId + "' ORDER BY history_id DESC limit 8;";

        // SELECTを実行し、結果表を取得
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
            int history_id = rs.getInt("history_id");
            String login_id = rs.getString("login_id");
            int price_total = rs.getInt("price_total");
            Date buy_day = rs.getTimestamp("buy_day");
            HistoryBeans cart = new HistoryBeans(history_id, login_id, price_total, buy_day);

            historyList.add(cart);
        }

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return historyList;
}

//----------------------------------------------------------------------------------------------------
//購入履歴　詳細1　表示

public HistoryBeans findHistorydetail1(int historyId) {
    Connection conn = null;

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM history WHERE history_id = ?";

        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1, historyId);
        ResultSet rs = pStmt.executeQuery();

        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }
    	// 必要なデータのみインスタンスのフィールドに追加
        int history_id = rs.getInt("history_id");
        String login_id = rs.getString("login_id");
        int price_total = rs.getInt("price_total");
        Date buy_day = rs.getTimestamp("buy_day");

        return new HistoryBeans(history_id, login_id, price_total, buy_day);

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------
//購入履歴　詳細2　表示
public List<HistoryDetailBeans> findHistoryDetail(int historyId) {
  Connection conn = null;

  List<HistoryDetailBeans> historyList = new ArrayList<HistoryDetailBeans>();

  try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * "
      		+ "FROM history_detail "
      		+ "INNER JOIN item "
      		+ "ON history_detail.item_id = item.item_id "
      		+ "WHERE history_id =" + historyId + ";";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
          int detail_id = rs.getInt("detail_id");
          int history_id = rs.getInt("history_id");
          int item_id = rs.getInt("item_id");
          int item_count = rs.getInt("item_count");

          String item_name = rs.getString("item_name");
          int item_price = rs.getInt("item_price");

          HistoryDetailBeans cart = new HistoryDetailBeans(detail_id, history_id, item_id, item_count, item_name, item_price);

          historyList.add(cart);
      }

  } catch (SQLException e) {
      e.printStackTrace();
      return null;
  } finally {
      // データベース切断
      if (conn != null) {
          try {
              conn.close();
          } catch (SQLException e) {
              e.printStackTrace();
              return null;
          }
      }
  }
  return historyList;
}

//----------------------------------------------------------------------------------------------------
//新着商品
public List<ItemBeans> newItemList() {
    Connection conn = null;
    List<ItemBeans> itemList = new ArrayList<ItemBeans>();

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        // TODO: 未実装：管理者以外を取得するようSQLを変更する
        String sql = "SELECT * FROM item ORDER BY item_id DESC limit 3;";

         // SELECTを実行し、結果表を取得
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
            int item_id = rs.getInt("item_id");
            String item_name = rs.getString("item_name");
            String item_text = rs.getString("item_text");
            int item_price = rs.getInt("item_price");
            int item_count = rs.getInt("item_count");
            String file_name = rs.getString("file_name");
            ItemBeans item = new ItemBeans(item_id, item_name, item_text, item_price, item_count, file_name);

            itemList.add(item);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return itemList;
}

//　-----------------------------------------------------------------------------------
//　購入数を在庫から引く　
public void stock(int item_id, int item_count) {
	Connection conn = null;
	PreparedStatement stmt = null;

    try {
    // データベースへ接続
    conn = DBManager.getConnection();

    String sql = "UPDATE item SET item_count = item_count - " + item_count + " WHERE item_id = ?;";

    stmt = conn.prepareStatement(sql);
    stmt.setInt(1, item_id);

    stmt.executeUpdate();
	stmt.close();

} catch (SQLException e) {
    e.printStackTrace();
} finally {
	try {
    // データベース切断
    if (conn != null) {
        conn.close();
    }
    } catch (SQLException e) {
        e.printStackTrace();
    }
}
}

// ---------------------------------------------------------------------------------------------
//欲しいものリスト 登録

public void wishItem(String login_id, int item_id) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
		 // データベースへ接続
		 conn = DBManager.getConnection();
		 // SELECT文を準備
		 String sql = "INSERT INTO wish_item(login_id, item_id) VALUES(?, ?);";

		 // SELECTを実行し、結果表を取得
		 stmt = conn.prepareStatement(sql);
		 stmt.setString(1, login_id);
		 stmt.setInt(2, item_id);

		 stmt.executeUpdate();
		 stmt.close();

		} catch (SQLException e) {
		 e.printStackTrace();
		} finally {
			try {
		 // データベース切断
		 if (conn != null) {
		     conn.close();
		 }
		 } catch (SQLException e) {
		     e.printStackTrace();
		 }
	 }
}

// --------------------------------------------------------------------------------------------------------
// 欲しいものリスト 表示

public List<ItemBeans> wishItemSeach(String login_id) {
    Connection conn = null;
    List<ItemBeans> wishList = new ArrayList<ItemBeans>();

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        // TODO: 未実装：管理者以外を取得するようSQLを変更する
        String sql = "SELECT * FROM wish_item INNER JOIN item ON wish_item.item_id = item.item_id WHERE login_id ='" + login_id + "';";

        // SELECTを実行し、結果表を取得
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
        	int wish_id = rs.getInt("wish_id");
            int item_id = rs.getInt("item_id");
            String item_name = rs.getString("item_name");
            String item_text = rs.getString("item_text");
            int item_price = rs.getInt("item_price");
            int item_count = rs.getInt("item_count");
            String file_name = rs.getString("file_name");
            ItemBeans item = new ItemBeans(wish_id, item_id, item_name, item_text, item_price, item_count, file_name);

            wishList.add(item);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return wishList;
}

//--------------------------------------------------------------------------------------------------------
//欲しいものリスト 削除

public void deleteItem(int a) {

	Connection conn = null;
	PreparedStatement stmt = null;

    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
        String sql = "DELETE FROM wish_item WHERE wish_id = ?;";

        // SELECTを実行し、結果表を取得
        stmt = conn.prepareStatement(sql);
        stmt.setInt(1, a);

        stmt.executeUpdate();
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}

//--------------------------------------------------------------------------------------------------------
// 人気の商品　TOP3

public List<ItemBeans> popularItemList() {
    Connection conn = null;
    List<ItemBeans> itemList = new ArrayList<ItemBeans>();

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT SUM(history_detail.item_count), history_detail.item_id, item.item_name," +
        		     "item.item_text, item.item_price, item.file_name " +
	        		 "FROM history_detail " +
	        	 	 "INNER JOIN item " +
	        		 "ON history_detail.item_id = item.item_id " +
	        		 "group by item_id " +
	        		 "ORDER BY SUM(history_detail.item_count) DESC " +
	        		 "LIMIT 3;";

        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {
            int item_id = rs.getInt("history_detail.item_id");
            String item_name = rs.getString("item.item_name");
            String item_text = rs.getString("item.item_text");
            int item_price = rs.getInt("item.item_price");
            String file_name = rs.getString("item.file_name");
            ItemBeans item = new ItemBeans(item_id, item_name, item_text, item_price, file_name);

            itemList.add(item);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return itemList;
}

//--------------------------------------------------------------------------------------------------------
//商品レビュー　登録

public void newReviews(int item_id, String reviews_title, String reviews_text, String user_name, String login_id) {
	Connection conn = null;
	PreparedStatement stmt = null;

		try {
		 conn = DBManager.getConnection();

		 String sql = "INSERT INTO reviews(item_id, reviews_title, reviews_text, user_name, login_id) VALUES(?, ?, ?, ?, ?);";

		 stmt = conn.prepareStatement(sql);
		 stmt.setInt(1, item_id);
		 stmt.setString(2, reviews_title);
		 stmt.setString(3, reviews_text);
		 stmt.setString(4, user_name);
		 stmt.setString(5, login_id);

		 stmt.executeUpdate();
		 stmt.close();

		} catch (SQLException e) {
		 e.printStackTrace();
		} finally {
			try {
		 // データベース切断
		 if (conn != null) {
		     conn.close();
		 }
		 } catch (SQLException e) {
		     e.printStackTrace();
	 }
   }
}


//--------------------------------------------------------------------------------------------------------
//商品レビュー　表示

public List<ReviewsBeans> reviewsList(int item_id) {
    Connection conn = null;
    List<ReviewsBeans> wishList = new ArrayList<ReviewsBeans>();

    try {
        conn = DBManager.getConnection();

        String sql = "SELECT * FROM reviews WHERE item_id =" + item_id + ";";

        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {
            int itemId = rs.getInt("item_id");
            String reviews_title = rs.getString("reviews_title");
            String review_text = rs.getString("reviews_text");
            String user_name = rs.getString("user_name");
            String login_id = rs.getString("login_id");

            ReviewsBeans item = new ReviewsBeans(itemId, reviews_title, review_text, user_name, login_id);

            wishList.add(item);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return wishList;
}


//--------------------------------------------------------------------------------------------------------
//ユーザーページ　商品レビュー　表示

public List<ReviewsBeans> userReviewsList(String login_id) {
  Connection conn = null;
  List<ReviewsBeans> wishList = new ArrayList<ReviewsBeans>();

  try {
      conn = DBManager.getConnection();

      String sql = "SELECT * "
	      		 + "FROM reviews "
	      		 + "INNER JOIN item "
	      		 + "ON reviews.item_id = item.item_id "
	      		 + "WHERE login_id ='" + login_id + "';";

      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
    	  int review_id = rs.getInt("review_id");
          int itemId = rs.getInt("item_id");
          String reviews_title = rs.getString("reviews_title");
          String review_text = rs.getString("reviews_text");
          String user_name = rs.getString("user_name");
          String loginId = rs.getString("login_id");
          String file_name = rs.getString("file_name");
          String item_name = rs.getString("item_name");
          int item_price = rs.getInt("item_price");

          ReviewsBeans item = new ReviewsBeans(review_id, itemId, reviews_title, review_text, user_name, loginId, file_name, item_name, item_price);

          wishList.add(item);
      }
  } catch (SQLException e) {
      e.printStackTrace();
      return null;
  } finally {
      // データベース切断
      if (conn != null) {
          try {
              conn.close();
          } catch (SQLException e) {
              e.printStackTrace();
              return null;
          }
      }
  }
  return wishList;
}

//--------------------------------------------------------------------------------------------------------
//商品レビュー　削除ページ

public void deleteReviewItem(int review_id) {

	Connection conn = null;
	PreparedStatement stmt = null;

    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
        String sql = "DELETE FROM reviews WHERE review_id = ?;";

        stmt = conn.prepareStatement(sql);
        stmt.setInt(1, review_id);

        stmt.executeUpdate();
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}


//--------------------------------------------------------------------------------------------------------
// 今月の売り上げ

public HistoryBeans monthSale(Date date) {
    Connection conn = null;

    try {
        conn = DBManager.getConnection();

    String sql = "SELECT SUM(history.price_total) " +
        		 "FROM history " +
        		 "JOIN history_detail " +
        		 "ON history.history_id = history_detail.history_id " +
        		 "WHERE buy_day ";

//    一月
//   カレンダーの取得
     Calendar cl = Calendar.getInstance();
//   フォーマット指定
     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

//   月初
     cl.set(Calendar.DATE, 1);
     String firstDate = sdf.format(cl.getTime());

//   月末
     int lastday = cl.getActualMaximum(Calendar.DAY_OF_MONTH);
     cl.set(Calendar.DATE, lastday);
     String endDate = sdf.format(cl.getTime());

//   sql文
     sql += "BETWEEN '" + firstDate +"' AND '" + endDate + "'";

      System.out.println(sql);
        PreparedStatement pStmt = conn.prepareStatement(sql);
        ResultSet rs = pStmt.executeQuery();

        if (!rs.next()) {
            return null;
        }

        int month_price = rs.getInt("SUM(history.price_total)");
        return new HistoryBeans(month_price);

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

//------------------------------------------------------------------------------------------
//コンタクトフォーム

public static void contact(String contact_category, String user_name, String user_address, String contact_text) {
	Connection conn = null;
	PreparedStatement stmt = null;

	try {
	 // データベースへ接続
	 conn = DBManager.getConnection();
	 // SELECT文を準備
	 String sql = "INSERT INTO contact(contact_category, user_name, user_address, contact_text, post_day) VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP);";

	 // SELECTを実行し、結果表を取得
	 stmt = conn.prepareStatement(sql);
	 stmt.setString(1, contact_category);
	 stmt.setString(2, user_name);
	 stmt.setString(3, user_address);
	 stmt.setString(4, contact_text);

	 stmt.executeUpdate();
	 stmt.close();

	} catch (SQLException e) {
	 e.printStackTrace();
	} finally {
		try {
	 // データベース切断
	 if (conn != null) {
	     conn.close();
	 }
	 } catch (SQLException e) {
	     e.printStackTrace();
	 }
 }
}

//--------------------------------------------------------------------------------------
//セール商品一覧　表示
public List<ItemBeans> saleItemAll() {
    Connection conn = null;
    List<ItemBeans> itemList = new ArrayList<ItemBeans>();

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        String sql = "SELECT * "
            	   + "FROM sale_item "
            	   + "INNER JOIN item "
            	   + "ON sale_item.item_id = item.item_id;";

        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {
            int item_id = rs.getInt("item_id");
            String item_name = rs.getString("item_name");
            String item_text = rs.getString("item_text");
            int item_price = rs.getInt("item_price");
            int item_count = rs.getInt("item_count");
            String file_name = rs.getString("file_name");
            ItemBeans item = new ItemBeans(item_id, item_name, item_text, item_price, item_count, file_name);

            itemList.add(item);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return itemList;
}








}










