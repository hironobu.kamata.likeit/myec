package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.ContactBeans;
import beans.ItemBeans;
import db.DBManager;

public class MasterDao {
//  ---------------------------------------------------------------------------------------------------------------------------------
//　新規登録
	public void newItem(String item_name, String item_text, int item_price, int item_count, String file_name, int category_id) {
		Connection conn = null;
		PreparedStatement stmt = null;

	    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
        String sql = "INSERT INTO item(item_name, item_text, item_price, item_count, file_name, category_id) VALUES(?, ?, ?, ?, ?, ?);";

        // SELECTを実行し、結果表を取得
        stmt = conn.prepareStatement(sql);
        stmt.setString(1, item_name);
        stmt.setString(2, item_text);
        stmt.setInt(3, item_price);
        stmt.setInt(4, item_count);
        stmt.setString(5, file_name);
        stmt.setInt(6, category_id);

        stmt.executeUpdate();
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}


//---------------------------------------------------------------------------------------------------------------------------------

//	アップデート 更新 商品
	public void updateItem(String item_name, String item_text, int item_price, int item_count, String file_name, int category_id, int item_id) {
		Connection conn = null;
		PreparedStatement stmt = null;

	    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        String sql = "UPDATE item SET item_name = ?, item_text = ?, item_price = ?, item_count = ?, file_name = ?, category_id = ? "
        		   + "WHERE item_id = ?;";

        stmt = conn.prepareStatement(sql);
        stmt.setString(1, item_name);
        stmt.setString(2, item_text);
        stmt.setInt(3, item_price);
        stmt.setInt(4, item_count);
        stmt.setString(5, file_name);
        stmt.setInt(6, category_id);
        stmt.setInt(7, item_id);

        stmt.executeUpdate();
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}

//--------------------------------------------------------------------------------------
// アイテム検索

public ItemBeans findItem(int id) {
	Connection conn = null;

    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
        String sql = "SELECT item_id, item_name, item_text, item_price, item_count, file_name, category_id FROM item WHERE item_id = ?";

        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1, id);
        ResultSet rs = pStmt.executeQuery();

        if (!rs.next()) {
            return null;
        }

     // 必要なデータのみインスタンスのフィールドに追加
        int item_id = rs.getInt("item_id");
        String item_name = rs.getString("item_name");
        String item_text = rs.getString("item_text");
        int item_price = rs.getInt("item_price");
        int item_count = rs.getInt("item_count");
        String file_name = rs.getString("file_name");
        int category_id = rs.getInt("category_id");

        ItemBeans itemDate = new ItemBeans(item_id, item_name, item_text, item_price, item_count, file_name, category_id);
        return itemDate;

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

//--------------------------------------------------------------------------------------
// カテゴリ　category_idからcategory_nameを検索

public static ItemBeans categorySearch(int category_id) {
	Connection conn = null;

    try {
        // データベースへ接続
        conn = DBManager.getConnection();
        // SELECT文を準備
        String sql = "SELECT category_name FROM category WHERE category_id = ?";

        // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1, category_id);
        ResultSet rs = pStmt.executeQuery();

        if (!rs.next()) {
            return null;
        }

     // 必要なデータのみインスタンスのフィールドに追加
        String category_name = rs.getString("category_name");

        ItemBeans itemDate = new ItemBeans(category_name);
        return itemDate;

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}

//--------------------------------------------------------------------------------------

//  商品一覧
	public List<ItemBeans> itemAll() {
        Connection conn = null;
        List<ItemBeans> itemList = new ArrayList<ItemBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = " SELECT * FROM item";

            // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int item_id = rs.getInt("item_id");
                String item_name = rs.getString("item_name");
                String item_text = rs.getString("item_text");
                int item_price = rs.getInt("item_price");
                int item_count = rs.getInt("item_count");
                String file_name = rs.getString("file_name");
                ItemBeans item = new ItemBeans(item_id, item_name, item_text, item_price, item_count, file_name);

                itemList.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return itemList;
    }

	//--------------------------------------------------------------------------------------
	// 商品削除
	public void deleteItem(int item_id) {

		Connection conn = null;
		PreparedStatement stmt = null;

	    try {
	        conn = DBManager.getConnection();

	        String sql = "DELETE FROM item WHERE item_id = ?;";

	        stmt = conn.prepareStatement(sql);
	        stmt.setInt(1, item_id);

	        stmt.executeUpdate();
	    	stmt.close();

	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	    	try {
	        // データベース切断
	        if (conn != null) {
	            conn.close();
	        }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
		}
	}

	//--------------------------------------------------------------------------------------
	// セールアイテム追加

	public void newSaleItem(int item_id, String item_name) {
		Connection conn = null;
		PreparedStatement stmt = null;

	    try {
        conn = DBManager.getConnection();

        String sql = "INSERT INTO sale_item(item_id, item_name) VALUES(?, ?);";

        stmt = conn.prepareStatement(sql);
        stmt.setInt(1, item_id);
        stmt.setString(2, item_name);

        stmt.executeUpdate();
    	stmt.close();

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
    	try {
        // データベース切断
        if (conn != null) {
            conn.close();
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}

	//--------------------------------------------------------------------------------------
	//セール商品一覧　表示
	public List<ItemBeans> saleItemAll() {
	    Connection conn = null;
	    List<ItemBeans> itemList = new ArrayList<ItemBeans>();

	    try {
	        // データベースへ接続
	        conn = DBManager.getConnection();

	        String sql = "SELECT * "
	            	   + "FROM sale_item "
	            	   + "INNER JOIN item "
	            	   + "ON sale_item.item_id = item.item_id;";

	        Statement stmt = conn.createStatement();
	        ResultSet rs = stmt.executeQuery(sql);

	        while (rs.next()) {
	            int item_id = rs.getInt("item_id");
	            String item_name = rs.getString("item_name");
	            String item_text = rs.getString("item_text");
	            int item_price = rs.getInt("item_price");
	            int item_count = rs.getInt("item_count");
	            String file_name = rs.getString("file_name");
	            ItemBeans item = new ItemBeans(item_id, item_name, item_text, item_price, item_count, file_name);

	            itemList.add(item);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
	    return itemList;
	}

//--------------------------------------------------------------------------------------
// 		セール商品　削除
	public void deleteSaleItem(int item_id) {

		Connection conn = null;
		PreparedStatement stmt = null;

	    try {
	        conn = DBManager.getConnection();

	        String sql = "DELETE FROM sale_item WHERE item_id = ?;";

	        stmt = conn.prepareStatement(sql);
	        stmt.setInt(1, item_id);

	        stmt.executeUpdate();
	    	stmt.close();

	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	    	try {
	        // データベース切断
	        if (conn != null) {
	            conn.close();
	        }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
		}
	}

//--------------------------------------------------------------------------------------
// 問合せ

	public List<ContactBeans> contactList() {
        Connection conn = null;
        List<ContactBeans> contactList = new ArrayList<ContactBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM contact";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
            	int contact_id = rs.getInt("contact_id");
                String contact_category = rs.getString("contact_category");
                String user_name = rs.getString("user_name");
                String user_address = rs.getString("user_address");
                String contact_text = rs.getString("contact_text");
                String post_day = rs.getString("post_day");

                ContactBeans contact = new ContactBeans(contact_id, contact_category, user_name, user_address, contact_text, post_day);

                contactList.add(contact);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return contactList;
    }

//--------------------------------------------------------------------------------------
// 問合せ idと一致するもの

		public ContactBeans contact(int contact_id) {
	        Connection conn = null;

	        try {
	            conn = DBManager.getConnection();

	            String sql = "SELECT * FROM contact where contact_id = ?";

	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setInt(1, contact_id);
	            ResultSet rs = pStmt.executeQuery();

	            while (!rs.next()) {
	                return null;
	            }

	         // 必要なデータのみインスタンスのフィールドに追加
	            int contactId = rs.getInt("contact_id");
	            String contact_category = rs.getString("contact_category");
	            String user_name = rs.getString("user_name");
	            String user_address = rs.getString("user_address");
	            String contact_text = rs.getString("contact_text");
	            String post_day = rs.getString("post_day");

	            return new ContactBeans(contactId, contact_category, user_name, user_address, contact_text, post_day);

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }
//--------------------------------------------------------------------------------------
// 問合せ 削除 idと一致するもの
		public static void deleteContact(int id) {
			Connection conn = null;
			PreparedStatement stmt = null;

		    try {
		        conn = DBManager.getConnection();

		        String sql = "DELETE FROM contact WHERE contact_id = ?;";

		        stmt = conn.prepareStatement(sql);
		        stmt.setInt(1, id);

		        stmt.executeUpdate();
		    	stmt.close();

		    } catch (SQLException e) {
		        e.printStackTrace();
		    } finally {
		    	try {
		        // データベース切断
		        if (conn != null) {
		            conn.close();
		        }
		        } catch (SQLException e) {
		            e.printStackTrace();
		        }
			}
		}

}












