package beans;

public class HistoryDetailBeans {

	private int detail_id;
	private int history_id;
	private int item_id;
	private int item_count;

	private String item_name;
	private int item_price;

	public HistoryDetailBeans(int history_id, int item_id, int item_count, String item_name) {
		this.history_id = history_id;
		this.item_id = item_id;
		this.item_count = item_count;
	}

	public HistoryDetailBeans(int detail_id, int history_id, int item_id, int item_count, String item_name, int item_price) {
		this.detail_id = detail_id;
		this.history_id = history_id;
		this.item_id = item_id;
		this.item_count = item_count;
		this.item_name = item_name;
		this.item_price = item_price;
	}

//	アクセサ ---------------------------------------


	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public int getItem_price() {
		return item_price;
	}

	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}

	public int getDetail_id() {
		return detail_id;
	}
	public void setDetail_id(int detail_id) {
		this.detail_id = detail_id;
	}
	public int getHistory_id() {
		return history_id;
	}
	public void setHistory_id(int history_id) {
		this.history_id = history_id;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public int getItem_count() {
		return item_count;
	}
	public void setItem_count(int item_count) {
		this.item_count = item_count;
	}




}
