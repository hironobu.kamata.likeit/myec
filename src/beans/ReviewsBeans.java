package beans;

public class ReviewsBeans {

	private int review_id;
	private int item_id;
	private String reviews_title;
	private String reviews_text;
	private String user_name;
	private String login_id;
	private String file_name;
	private String item_name;
	private int item_price;

	public ReviewsBeans(int item_id, String reviews_title, String reviews_text, String user_name, String login_id) {
		this.item_id = item_id;
		this.reviews_title = reviews_title;
		this.reviews_text = reviews_text;
		this.user_name = user_name;
		this.login_id = login_id;
	}

	public ReviewsBeans(int review_id, int item_id, String reviews_title, String reviews_text, String user_name, String login_id, String file_name, String item_name, int item_price) {
		this.review_id = review_id;
		this.item_id = item_id;
		this.reviews_title = reviews_title;
		this.reviews_text = reviews_text;
		this.user_name = user_name;
		this.login_id = login_id;
		this.file_name = file_name;
		this.item_name = item_name;
		this.item_price = item_price;
	}


//  アクセサ

	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public String getReviews_title() {
		return reviews_title;
	}
	public void setReviews_title(String reviews_title) {
		this.reviews_title = reviews_title;
	}
	public String getReviews_text() {
		return reviews_text;
	}
	public void setReviews_text(String reviews_text) {
		this.reviews_text = reviews_text;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public int getItem_price() {
		return item_price;
	}
	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}
	public int getReview_id() {
		return review_id;
	}
	public void setReview_id(int review_id) {
		this.review_id = review_id;
	}

}
