package beans;

public class ItemBeans {

	private int item_id;
	private String item_name;
	private String item_text;
	private int item_price;
	private int item_count;
	private String file_name;

	private int category_id;
	private String category_name;
	private int wish_id;


	// 	コンストラクタ ---------------------------------------
	public ItemBeans(int item_id, String item_name, String item_text, int item_price, int item_count, String file_name) {
		this.item_id = item_id;
		this.item_name = item_name;
		this.item_text = item_text;
		this.item_price = item_price;
		this.item_count = item_count;
		this.file_name = file_name;
	}

	public ItemBeans(int item_id, String item_name, String item_text, int item_price, int item_count, String file_name, int category_id) {
		this.item_id = item_id;
		this.item_name = item_name;
		this.item_text = item_text;
		this.item_price = item_price;
		this.item_count = item_count;
		this.file_name = file_name;
		this.category_id = category_id;
	}

	public ItemBeans(int item_id, String item_name, String item_text, int item_price, String file_name) {
		this.item_id = item_id;
		this.item_name = item_name;
		this.item_text = item_text;
		this.item_price = item_price;
		this.file_name = file_name;
	}

	public ItemBeans(int wish_id, int item_id, String item_name, String item_text, int item_price, int item_count, String file_name) {
		this.wish_id = wish_id;
		this.item_id = item_id;
		this.item_name = item_name;
		this.item_text = item_text;
		this.item_price = item_price;
		this.item_count = item_count;
		this.file_name = file_name;
	}

	public ItemBeans(String item_name, String item_text, int item_price, int item_count, String file_name, int item_id) {
		this.item_name = item_name;
		this.item_text = item_text;
		this.item_price = item_price;
		this.item_count = item_count;
		this.file_name = file_name;
		this.item_id = item_id;
	}

	public ItemBeans(String item_name, String item_text, int item_price, int item_count, String file_name) {
		this.item_name = item_name;
		this.item_text = item_text;
		this.item_price = item_price;
		this.item_count = item_count;
		this.file_name = file_name;
	}

	public ItemBeans(int item_id, String item_name, int item_price, int item_count) {
		this.item_id = item_id;
		this.item_name = item_name;
		this.item_price = item_price;
		this.item_count = item_count;
	}


	public ItemBeans(String category_name) {
		this.category_name = category_name;
	}

//	アクセサ ---------------------------------------

	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public String getItem_text() {
		return item_text;
	}
	public void setItem_text(String item_text) {
		this.item_text = item_text;
	}
	public int getItem_price() {
		return item_price;
	}
	public void setItem_price(int item_price) {
		this.item_price = item_price;
	}
	public int getItem_count() {
		return item_count;
	}
	public void setItem_count(int item_count) {
		this.item_count = item_count;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public int getWish_id() {
		return wish_id;
	}
	public void setWish_id(int wish_id) {
		this.wish_id = wish_id;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public int getSale_price() {
		return (int)(this.item_price * 0.7);
	}






}
