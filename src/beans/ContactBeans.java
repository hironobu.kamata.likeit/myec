package beans;

public class ContactBeans {

	private int contact_id;
	private String contact_category;
	private String user_name;
	private String user_address;
	private String contact_text;
	private String post_day;

	public ContactBeans(int contact_id, String contact_category, String user_name, String user_address, String contact_text, String post_day) {
		this.contact_id = contact_id;
		this.contact_category = contact_category;
		this.user_name = user_name;
		this.user_address = user_address;
		this.contact_text = contact_text;
		this.post_day = post_day;
	}

	public ContactBeans(int contact_id) {
		this.contact_id = contact_id;
	}

	public ContactBeans(String contact_category, String user_name, String user_address, String contact_text, String post_day) {
		this.contact_category = contact_category;
		this.user_name = user_name;
		this.user_address = user_address;
		this.contact_text = contact_text;
		this.post_day = post_day;
	}

//	アクセサ　----------------------------------------------------

	public int getContact_id() {
		return contact_id;
	}
	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}
	public String getPost_day() {
		return post_day;
	}
	public void setPost_day(String post_day) {
		this.post_day = post_day;
	}
	public String getContact_category() {
		return contact_category;
	}
	public void setContact_category(String contact_category) {
		this.contact_category = contact_category;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_address() {
		return user_address;
	}
	public void setUser_address(String user_address) {
		this.user_address = user_address;
	}
	public String getContact_text() {
		return contact_text;
	}
	public void setContact_text(String contact_text) {
		this.contact_text = contact_text;
	}








}
