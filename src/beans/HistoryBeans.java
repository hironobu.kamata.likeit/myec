package beans;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HistoryBeans {

	private int history_id;
	private String login_id;
	private int price_total;
	private Date buy_day;

	private int month_price;

	public int getMonth_price() {
		return month_price;
	}

	public void setMonth_price(int month_price) {
		this.month_price = month_price;
	}

	public Date getBuy_day() {
		return buy_day;
	}

	public HistoryBeans(int history_id, String login_id, int price_total, Date buy_day) {
		this.history_id = history_id;
		this.login_id = login_id;
		this.price_total = price_total;
		this.buy_day = buy_day;
	}

	public HistoryBeans(int month_price) {
		this.month_price = month_price;
	}

//	アクセサ ---------------------------------------

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public void setPrice_total(int price_total) {
		this.price_total = price_total;
	}

	public void setBuy_day(Date buy_day) {
		this.buy_day = buy_day;
	}

	public int getHistory_id() {
		return history_id;
	}
	public void setHistory_id(int history_id) {
		this.history_id = history_id;
	}
	public String getUser_id() {
		return login_id;
	}
	public void setUser_id(String user_id) {
		this.login_id = user_id;
	}
	public int getPrice_total() {
		return price_total;
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(buy_day);
	}




}
