package beans;

public class CartBeans {

	private int id;
	private String name;
	private int count;
	private int price;
	private int countPrice;
	private int totalPrice;

	public CartBeans(int id, String name, int count, int price, int countPrice, int totalPrice) {
		this.id = id;
		this.name = name;
		this.count = count;
		this.price = price;
		this.countPrice = countPrice;
		this.totalPrice = totalPrice;
	}

	public int getCountPrice() {
		return countPrice;
	}

	public void setCountPrice(int countPrice) {
		this.countPrice = countPrice;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	// アクセサ
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
