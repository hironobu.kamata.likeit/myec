package beans;

public class UserBeans {

	private int user_id;
	private String user_name;
	private String login_id;
	private String password;

// 	コンストラクタ ---------------------------------------
	public UserBeans(String user_name, String login_id, String password) {
		this.user_name = user_name;
		this.login_id = login_id;
		this.password = password;
	}

	public UserBeans(String user_name, String login_id) {
		this.user_name = user_name;
		this.login_id = login_id;
	}

//	アクセサ ---------------------------------------

	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}



}
