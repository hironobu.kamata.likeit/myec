<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- ユーザーページ -->

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>comfirm cart</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/user.css">
</head>

<body>

<div id="box"></div>

<header>
  <div id="header_content">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>

    <ul id="list">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </ul>
  </div>
</header>

<main>
<div id="box2">

<!-- 左右 clear -->
<div id="left_right">

    <!-- 購入履歴 左 -->
    <div id="left">

        <p id="hello-text">こんにちは、${userName }さん</p>

      <!-- 購入履歴 -->
      <div id="history">
        <h2>購入履歴</h2>

        <table>

         <tr>
            <th>購入日時</th>
            <th>購入金額</th>
            <th></th>
          </tr>

        <c:forEach var="history" items="${history_list}" varStatus="status">
          <tr>
            <td>${history.formatDate }</td>
            <td>${history.price_total } 円</td>
            <td>
            	<a href="UserDetailServlet?historyId=${history.history_id }">詳細</a>
            </td>
          </tr>
 		</c:forEach>

        </table>
      </div>

    </div>


    <!-- アカウント編集・削除 右 -->
    <div id="right">

        <!-- アカウント編集 -->
        <div id="change">
          <h2>アカウント情報編集</h2>
          <c:if test="${errMsg != null}">
	      	<div class="err-text">${errMsg}</div>
	  	  </c:if>
          <form action="UserServlet" method="post">
          	<input class="inp" type="hidden" name="login_id" value="${userInfo.login_id }">
            <input class="inp" type="text" name="user_name" value="${userName }" placeholder="user_name">
            <input class="inp" type="password" name="password" placeholder="password">
            <input class="inp" type="password" name="password2" placeholder="password comfirm">
            <input class="inp" type="submit" value="変更">
          </form>
        </div>

        <!-- アカウント削除 -->
        <div id="delete"><a href="DeleteServlet">アカウントを削除する</a></div>

    </div>

</div>

</div>

<div id="Wish_list">
	<h2>欲しいものリスト</h2>
	<c:if test="${err1 != null}">
	    <div>${err1}</div>
	</c:if>
	 <ul>
		 <c:forEach var="item" items="${wishItemDate}" varStatus="status">
			<li class="ul_list">
			  <a href="ProductDetailServlet?id=${item.item_id }">
				<img class="widh-pic" src="img/${item.file_name }">
			  </a>
				<p>${item.item_name }</p>
				<p>${item.item_price } 円</p>
			    <div id="wish-form">

 				  <div id="in-cart">
				  	<a class="with-button" href="ProductDetailServlet?id=${item.item_id }">詳細</a>
				  </div>

 			      <div id="list-delete" class="click-button">
				  	<a class="with-button">削除</a>
                  </div>

			<div class="message">
				<p> ${item.item_name }を削除しますか？</p>

				<div id="clear">
					<form id="delete-box" action="WishItemDelete" method="post">
                        <input type="hidden" name="wish_id" value="${item.wish_id }">
						<button id="deleteButton" class="with-button" type="submit">削除</button>
					</form>

					<div class="back-box">
						<a id="deleteButton" class="with-button back-button">戻る</a>
					</div>
				</div>
			</div>

			    </div>
			</li>
		</c:forEach>
	 </ul>
</div>



<div id="review-box">

	<h2>投稿したレビュー</h2>
	<c:if test="${err2 != null}">
	    <div>${err2}</div>
	</c:if>

	<c:forEach var="review" items="${userReview}" varStatus="status">
		<div id="review-content">
			<div id="review-item-date">
				<a href="ProductDetailServlet?id=${review.item_id }"><img src="img/${review.file_name }"></a>
				<p>商品名 : ${review.item_name }</p>
				<p>金額 : ${review.item_price } 円</p>
			</div>
			<div id="review-written">
				<p id="title">${review.reviews_title }</p>
				<p>${review.reviews_text }</p>
			</div>

            <div id="review-delete">
				<a href="DeleteReviewServlet?review_id=${review.review_id }">このレビューを削除する</a>
			</div>

		</div>
	</c:forEach>

</div>

</main>

<script src="js/user.js"></script>
</body>
</html>








