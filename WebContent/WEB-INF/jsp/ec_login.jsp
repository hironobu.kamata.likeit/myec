<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- ログインページ -->

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>LOGIN</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/ec_login.css">
</head>
<body>

<header>

  <div id="header_content">

    <h1 id="EC"><a href="IndexServlet">EC</a></h1>

    <ul id="list">
      <li><a href="EcLoginServlet">LOGIN</a></li>
      <li><a href="NewUserServlet">SIGN UP</a></li>
    </ul>

  </div>

</header>

<main>
<!-- フォーム -->
<div id="login_content">
    <h2>LOGIN</h2>
      <c:if test="${errMsg != null}">
	    <div class="err-text">${errMsg}</div>
	  </c:if>

    <form action="EcLoginServlet" method="post">
      <input id="user_id" type="text" name="login_id" placeholder="login id">
      <input id="password" type="password" name="password" placeholder="password">

      <input id="submit" type="submit" value="login">
    </form>
</div>

<div id="newAccount"><a href="NewUserServlet">アカウントを作成する</a></div>

</main>

<div id="admin_login"><a href="AdminLoginServlet">管理者ログイン</a></div>

</body>
</html>
