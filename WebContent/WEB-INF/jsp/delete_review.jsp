<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="charset=UTF-8">
<title>delete review</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/delete.css">
</head>
<body>

<header>
  <div id="header_content">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>

    <ul id="list">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </ul>
  </div>
</header>

<main>

  <div id="delete_box">
    <h2>投稿内容を削除</h2>

    <div id="user_id">
      <p>レビューを削除しますか？</p>
    </div>

    <div id="form">
      <form action="UserServlet">
        <input class="padd" type="submit" name="back" value="戻る">
      </form>

      <form action="DeleteReviewServlet" method="post">
      	<input class="inp" type="hidden" name="review_id" value="${reviewId }">
        <input class="padd" type="submit" name="delete" value="削除">
      </form>
    </div>

  </div>



</main>

</body>
</html>