<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>問合せ一覧</title>
<link rel="stylesheet" href="admin_css/common.css">
<link rel="stylesheet" href="admin_css/admin-contact.css">
</head>
<body>

<main>

	<h2>問合せ一覧</h2>

	<c:if test="${message != null}">
		${message }
	</c:if>

  <table>
    <tr>
        <th>お問合せ内容</th>
        <th>お名前</th>
        <th>日付</th>
        <th></th>
      </tr>
      <c:forEach var="con" items="${contact }">
	      <tr>
	        <td>${con.contact_category }</td>
	        <td>${con.user_name }</td>
	        <td>${con.post_day }</td>
	        <td>
	          <a href="AdminContactDetailServlet?id=${con.contact_id }">詳細</a>
	        </td>
	      </tr>
      </c:forEach>
    </table>

	<a id="back" href="AdminTopServlet">戻る</a>

</main>

</body>
</html>