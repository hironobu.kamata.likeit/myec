<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- 商品一覧ページ -->

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>ProductList</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/product_list.css">
</head>
<body>

<header>

  <div id="header_content">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>

    <ul id="list">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </ul>
  </div>
</header>

<main>

<!-- 検索フォーム -->
<div id="product_form">

  <form action="ProductListServlet" method="post">
    <input id="product_search" type="text" name="serch_name">
	<div>
		<select name="select_serch">
			<option value="0">全て</option>
			<option value="1">机・デスク</option>
			<option value="2">椅子・ソファ</option>
			<option value="3">収納・タンス</option>
			<option value="4">照明・ライト</option>
		</select>

	    <input type="submit" value="検索">
    </div>
  </form>

</div>

<!-- 商品一覧 -->

<c:if test="${itemSerch != null}">
	<c:if test="${itemSaleList != null}">
		<h2 id="sale-product">TIME SALE ITEM</h2>

		<div class="product_list">

			<c:forEach var="sale" items="${itemSaleList}">
		 	    <div class="product_content">
			      <a href="SaleItemDetailServlet?id=${sale.item_id }">
			        <h3>${sale.item_name }</h3>
			        <div class="img-box">
			        <img class="product_pic" src="img/${sale.file_name }">
			        </div>
			        <p class="product_text">${sale.item_text }</p>
			        <p class="product_price">
			        セール価格 ${sale.sale_price} 円
			        </p>
			      </a>
			    </div>
			 </c:forEach>

		</div>
	</c:if>
</c:if>

<!-- 商品一覧 -->

<c:if test="${itemSerch != null}">
	<h2 id="popular-product">BEST SELLERS</h2>

	<div class="product_list">

		<c:forEach var="popular" items="${popularItem}">

	 	    <div class="product_content">
		      <a href="ProductDetailServlet?id=${popular.item_id }">
		        <h3>${popular.item_name }</h3>
		        <div class="img-box">
		        <img class="product_pic" src="img/${popular.file_name }">
		        </div>
		        <p class="product_text">${popular.item_text }</p>
		        <p class="product_price">${popular.item_price } 円</p>
		      </a>
		    </div>
		 </c:forEach>

	</div>
</c:if>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

<!-- 商品一覧 -->

<h2 id="product">PRODUCT</h2>

<!-- 検索した商品がない時のメッセージ -->
<c:if test="${message != null}">
	<div id="message-box">${message}</div>
</c:if>

<c:if test="${itemList != null}">
	<div class="product_list">

	 <c:forEach var="item" items="${itemList}">
	    <div class="product_content">
	      <a href="ProductDetailServlet?id=${item.item_id }">
	        <h3>${item.item_name }</h3>
	        <div class="img-box">
	        <img class="product_pic" src="img/${item.file_name }">
	        </div>
	        <p class="product_text">${item.item_text }</p>
	        <p class="product_price">${item.item_price } 円</p>
	      </a>
	    </div>
	 </c:forEach>
</c:if>

</div>

<c:if test="${itemSerch != null}">
	<div class="product_list">

	 <c:forEach var="item" items="${itemSerch}">
	    <div class="product_content">
	      <a href="ProductDetailServlet?id=${item.item_id }">
	        <h3>${item.item_name }</h3>
	        <div class="img-box">
	        <img class="product_pic" src="img/${item.file_name }">
	        </div>
	        <p class="product_text">${item.item_text }</p>
	        <p class="product_price">${item.item_price } 円</p>
	      </a>
	    </div>
	 </c:forEach>

	</div>
</c:if>

<!-- ////// 商品一覧 -->

</main>



</body>
</html>
