<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- カート確認ページ -->

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>comfirm cart</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/comfirm.css">
<script src="js/main.js"></script>
</head>
<body>

<header>
  <div id="header_content">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>

    <ul id="list">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </ul>
  </div>
</header>

<main>

  <!-- ボタン -->
  <div id="button_box">

    <div id="return">
      <a class="comfirm_button" href="ProductListServlet">買い物に戻る</a>
    </div>

    <div id="purchase">
      <a class="comfirm_button" href="BuyComfirmServlet">購入手続きへ</a>
    </div>

  </div>

  <!-- テーブル -->
  <div id="cart-content">

    <table id="cart_table">

      <tr>
        <th>商品ID</th>
        <th>商品名</th>
        <th>単価</th>
        <th>個数</th>
        <th></th>
      </tr>

	 <div id="cartMessage">
	 	${cartMessage}
	 </div>

	 <c:forEach var="item" items="${cartItem}" varStatus="status">
	      <tr>
	        <td>${item.id } </td>
	        <td>${item.name } </td>
	        <td>${item.price } 円</td>
	        <td>${item.count }</td>
	        <td>
	          <form id="count" action="CartItemDelete" method="post">
	            <input type="checkbox" name="item_check" value="${item.id }">
	            <input type="submit" name="delete" value="削除">
	          </form>
	        </td>
	      </tr>
  	  </c:forEach>
    </table>
  </div>

</main>


</body>
</html>
