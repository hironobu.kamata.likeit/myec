<!-- トップページ -->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>EC TOP</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/top.css">
</head>
<body>

  <header>

    <div id="header_content">
      <h1 id="EC"><a href="IndexServlet">EC</a></h1></a>

      <ul id="list">
        <li><a href="EcLoginServlet">LOGIN</a></li>
        <li><a href="NewUserServlet">SIGN UP</a></li>
      </ul>
    </div>

  </header>

  <main>

    <div id="main_image">
      <img src="img/living.jpg">
    </div>

    <!-- 新商品 -->

<h2 id="arrivals">NEW ARRIVALS</h2>

<div class="product_list">

<c:forEach var="item" items="${itemDate}">
    <div class="product_content">
      <a href="ProductDetailServlet?id=${item.item_id }">
        <h3>${item.item_name }</h3>
        <img class="product_pic" src="img/${item.file_name }">
        <p class="product_text">${item.item_text }</p>
        <p class="product_price">${item.item_price } 円</p>
      </a>
    </div>
 </c:forEach>

</div>

<!-- - - - - - - - - - - - - - - - - - -  -->

<h2 id="product">PRODUCT</h2>

<div class="product_list">

<c:forEach var="item" items="${itemList}">
    <div class="product_content">
      <a href="ProductDetailServlet?id=${item.item_id }">
        <h3>${item.item_name }</h3>
        <img class="product_pic" src="img/${item.file_name }">
        <p class="product_text">${item.item_text }</p>
        <p class="product_price">${item.item_price } 円</p>
      </a>
    </div>
 </c:forEach>

</div>

</main>

</body>
</html>
