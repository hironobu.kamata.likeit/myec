<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>top</title>
<link rel="stylesheet" href="admin_css/common.css">
<link rel="stylesheet" href="admin_css/admin_top.css">
</head>
<body>

<main>

<div id="clear-box">

      <div id="left-box">
        <h2 id="admin">管理画面</h2>
		<div id="month_price">
			<dl>
				<dt>今月の売り上げ</dt>
				<dd>${monthPrice.month_price } 円</dd>
			</dl>
        </div>
      </div>

        <div id="link-box">
          <a href="LogoutServlet">ログアウト</a>
          <a href="AdminAddProductServlet">商品追加</a>
          <a href="AdminSaleServlet">セール商品管理</a>
          <a href="AdminContactServlet">お問い合わせ確認</a>
        </div>
</div>

  <h2 id="product">PRODUCT</h2>

  <div id="product_list">

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<c:forEach var="item" items="${itemList}">

      <div class="product_content">
          <h3>${item.item_name}</h3>
          <img class="product_pic" src="img/${item.file_name}">
          <p class="product_text">${item.item_text}</p>
          <p class="product_price">${item.item_price} 円</p>

          <div id="admin-form-product">

              <a class="admin_button" href="AdminUpdateServlet?id=${item.item_id}">更新</a>

              <a class="admin_button" href="AdminDetailServlet?id=${item.item_id}">詳細</a>

              <a class="admin_button" href="AdminDeleteProduct?id=${item.item_id}">削除</a>

          </div>
      </div>

 </c:forEach>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

</div>
</main>

</body>
</html>

