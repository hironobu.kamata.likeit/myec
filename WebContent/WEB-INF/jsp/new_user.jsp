<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 新規アカウント登録 -->

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>SIGN UP</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/new_user.css">
</head>
<body>

<header>

<div id="header_content">
  <h1 id="EC"><a href="IndexServlet">EC</a></h1>

  <ul id="list">
    <li><a href="EcLoginServlet">LOGIN</a></li>
    <li><a href="NewUserServlet">SIGN UP</a></li>
  </ul>
</div>

</header>

<main>
<!-- フォーム -->
<div id="new_user_content">
  <h2>SIGN UP</h2>

  <c:if test="${errMsg != null}">
   <div class="err-text">${errMsg}</div>
  </c:if>

  <form action="NewUserServlet" method="post">
    <input class="inp" type="text" name="user_name" placeholder="user name">
    <input class="inp" type="text" name="login_id" placeholder="login id">
    <input class="inp" type="password" name="password" placeholder="password">
    <input class="inp" type="password" name="password2" placeholder="confirm password">

    <input id="submit" type="submit">
  </form>
</div>

</main>



</body>
</html>
