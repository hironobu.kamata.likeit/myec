<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 削除確認 -->

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>delete comfirm</title>
  <link rel="stylesheet" href="css/common.css">
  <link rel="stylesheet" href="css/delete.css">
</head>
<body>

<header>
  <div id="header_content">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>

    <ul id="list">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </ul>
  </div>
</header>

<main>

  <div id="delete_box">
    <h2>アカウントを削除</h2>

    <div id="user_id">
      <p>ログインID : ${userInfo.login_id }<br>このアカウントを削除しますか？</p>
    </div>

    <div id="form">
      <form action="UserServlet">
        <input class="padd" type="submit" name="back" value="戻る">
      </form>

      <form action="DeleteServlet" method="post">
      	<input class="inp" type="hidden" name="login_id" value="${userInfo.login_id }">
        <input class="padd" type="submit" name="delete" value="削除">
      </form>
    </div>

  </div>



</main>

</body>
</html>
