<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>add product</title>
<link rel="stylesheet" href="admin_css/common.css">
<link rel="stylesheet" href="admin_css/add_product.css">
</head>
<body>

<main>

  <div id="add_product">

    <h2>商品情報追加</h2>

    <form action="AdminAddProductServlet" method="post">

      商品名<br><input type="text" name="item_name"><br>
      商品説明<br><textarea name="item_text" rows="4" cols="40">ここに記入してください。</textarea><br>
      金額<br><input type="text" name="item_price"><br>
      個数<br><input type="number" min="0" name="item_count"><br>
      商品カテゴリー<br>
   	  <select name="category_id">
		<option value="1">机・デスク</option>
		<option value="2">椅子・ソファ</option>
		<option value="3">収納・タンス</option>
		<option value="4">照明・ライト</option>
	  </select><br>
      商品画像<br><input type="file" name="file_name"><br>

      <input id="subm" type="submit" value="追加">
    </form>

  </div>

<form id="back_button" action="AdminTopServlet">
  <input type="submit" name="back" value="戻る">
</form>

</main>

</body>
</html>
