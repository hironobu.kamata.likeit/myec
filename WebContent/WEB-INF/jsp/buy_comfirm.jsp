<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 購入確認ページ -->

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>購入</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/buy_comfirm.css">
</head>
<body>

<header>
  <div id="header_content">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>

    <ul id="list">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </ul>
  </div>
</header>

<main>
    <!-- ボタン -->
    <div id="button_box">
      <div id="buy">
	      <form action="BuyComfirmServlet" method="post">
	   		<input class="inp" type="hidden" name="login_id" value="${userInfo.login_id }">
	   		<input class="inp" type="hidden" name="total_price" value="${totalPrice }">

	       	<button class="comfirm_button">購入</button>
	      </form>
      </div>
    </div>

    <!-- テーブル -->
    <div id="buy-content">

      <table id="buy_table">

        <tr>
          <th>商品ID</th>
          <th>商品名</th>
          <th>個数</th>
          <th>金額</th>
        </tr>

<c:forEach var="item" items="${cartItem}" varStatus="status">
        <tr>
          <td>${item.id }</td>
          <td>${item.name }</td>
          <td>${item.count }</td>
          <td>${item.countPrice } 円</td>
        </tr>
 </c:forEach>
      </table>
    </div>

    <!-- 合計 -->
    <div id="total">
      <div id="total_price">
        <p>${totalPrice } 円</p>
      </div>
    </div>
</main>



</body>
</html>
