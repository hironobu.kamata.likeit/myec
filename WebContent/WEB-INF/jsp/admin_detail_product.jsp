<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>detail product</title>
<link rel="stylesheet" href="admin_css/common.css">
<link rel="stylesheet" href="admin_css/admin_detail_product.css">
</head>
<body>

<main>

  <h2>商品詳細情報</h2>

  <div id="detail_content">

    <div id="detail_img">
      <img class="product_pic" src="img/${itemDate.file_name }">
    </div>

    <div id="detail_text">
      <p>商品ID: ${itemDate.item_id }</p>
      <h3>商品名: ${itemDate.item_name }</h3>
      <p id="product_text">商品説明: ${itemDate.item_text }</p>
      <p>単価 ${itemDate.item_price } 円</p>
      <p>在庫数: ${itemDate.item_count } 個</p>
	   <c:if test="${errMsg != null}" >
	    <div class="err-text">${errMsg}</div>
	   </c:if>
	   <p>商品カテゴリ: ${categoryName.category_name }</p>
    </div>

  </div>

  <form id="back_button" action="AdminTopServlet">
    <input type="submit" name="back" value="戻る">
  </form>


</main>

</body>
</html>
