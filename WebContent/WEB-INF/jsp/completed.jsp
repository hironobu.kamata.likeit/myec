<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 購入完了ページ -->

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>comfirm cart</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/complete.css">
</head>
<body>

<header>
  <div id="header_content">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>

    <ul id="list">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </ul>
  </div>
</header>

<main>

<div id="buy_completed">

    <p>購入が完了しました</p>

    <div id="button_box">
      <div id="left">
        <a class="button" href="ProductListServlet">商品一覧</a>
      </div>
      <div id="right">
        <a class="button" href="UserServlet">ユーザーページ</a>
      </div>
    </div>

</div>

</main>


<body>
</html>
