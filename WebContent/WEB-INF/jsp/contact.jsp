<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>contact</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/contact.css">
</head>
<body>

<header>
  <div id="header_content">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>

    <ul id="list">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </ul>
  </div>
</header>

<main>

 <h2>CONTACT</h2>


<div id="frame-bx">

  <form action="ContactServlet" method="post">

    <h3>お問い合わせ内容を選んでください</h3>
    <div id="select">
      <label class="lab">お支払いに関して <input type="radio" name="contact_check" value="お支払いについて" checked></label><span class="spa"></span>
      <label class="lab lab2">会員登録に関して <input type="radio" name="contact_check" value="会員登録に関して"></label><span class="spa"></span>
      <label class="lab lab2">返品・交換に関して <input type="radio" name="contact_check" value="返品交換"></label><span class="spa"></span>
      <label class="lab lab2">その他 <input type="radio" name="contact_check" value="その他"></label>
    </div>

    <div id="box">
        <label>お名前 :　<input id="nm-box" class="nr-form" type="text" name="name" ><br></label>
        <label>連絡先 :　<input class="nr-form" type="text" name="address" ><br></label>
    </div>

    <textarea name="text" rows="8" cols="73" placeholder="本文"></textarea>

	<c:if test="${err != null}">
		<div id="err" class="err-text">${err }</div>
	</c:if>

    <button id="send" type="submit" name="contact_form">送信</button>

  </form>

</div>

<a id="back" href="ProductListServlet">戻る</a>

</main>


</body>
</html>