<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 商品詳細 -->

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>ProductList</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/product_detail.css">
</head>
<body>

<header>
  <div id="header_content">

  <c:if test="${userInfo != null}">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>
  </c:if>

  <c:if test="${userInfo == null}">
    <h1 id="EC"><a href="IndexServlet">EC</a></h1>
  </c:if>

    <ul id="list">
    <c:if test="${userInfo != null}">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </c:if>

    <c:if test="${userInfo == null}">
      <li><a href="EcLoginServlet">LOGIN</a></li>
      <li><a href="NewUserServlet">SIGN UP</a></li>
    </c:if>
    </ul>
  </div>
</header>

<main>

<div id="detail_content">
	<!-- 通常価格 -->
	<c:if test="${itemDate != null}">
	    <div id="detail_img">
	      <img class="product_pic" src="img/${itemDate.file_name }">
	    </div>
	    <div id="detail_text">
	      <h3>${itemDate.item_name }</h3>
	      <p id="product_text">${itemDate.item_text }</p>
	      	<p>${itemDate.item_price } 円</p>
	    </div>
	</c:if>

	<!-- セール価格 -->
	<c:if test="${itemSaleDate != null}">
		<div id="detail_img">
	      <img class="product_pic" src="img/${itemSaleDate.file_name }">
	    </div>
	    <div id="detail_text">
	      <h3>${itemSaleDate.item_name }</h3>
	        <p id="product_text">${itemSaleDate.item_text }</p>
	      	<p>セール価格 ${itemSaleDate.sale_price } 円</p>
	    </div>
	</c:if>

</div>

  <!-- カート -->
  <!-- ユーザー情報がある場合 -->
  <c:if test="${userInfo != null}">
	  <div id="cart">
	    <form id="cart_form" action="ComfirmCartServlet" method="post">
	    <c:if test="${itemDate != null}">
	      	<input type="hidden" name="id" value="${itemDate.item_id }">
	      	<input type="hidden" name="name" value="${itemDate.item_name }">
	      	<input type="hidden" name="price" value="${itemDate.item_price }">
	      </c:if>
	      <c:if test="${itemSaleDate != null}">
	      	<input type="hidden" name="id" value="${itemSaleDate.item_id }">
	      	<input type="hidden" name="name" value="${itemSaleDate.item_name }">
	      	<input type="hidden" name="price" value="${itemSaleDate.sale_price }">
	      </c:if>
	  個数 <input id="count-input" type="number" min="1" max="10" value="1" name="item_count">
	      <input class="submit" type="submit" value="カートに入れる">
	    </form>

		  <div id="wish">
			<form action="WishServlet" method="post">
			<c:if test="${itemDate != null}">
				<input type="hidden" name="id" value=${itemDate.item_id }>
			</c:if>
			<c:if test="${itemSaleDate != null}">
				<input type="hidden" name="id" value=${itemSaleDate.item_id }>
			</c:if>
			  	<input class="submit" type="submit" value="欲しいものリストに追加">
			</form>
		  </div>

		  <div id="write-review">
			<form action="WriteReviewServlet" method="post">
			<c:if test="${itemDate != null}">
				<input type="hidden" name="id" value=${itemDate.item_id }>
			</c:if>
			<c:if test="${itemSaleDate != null}">
				<input type="hidden" name="id" value=${itemSaleDate.item_id }>
			</c:if>
			  	<input class="submit" type="submit" value="レビューを書く">
			</form>
		  </div>
	  </div>
  </c:if>

<!-- ユーザー情報がない場合 -->
  <c:if test="${userInfo == null}">
	  <div id="cart">
	    <form id="cart_form" action="FromTopComfirmCartServlet" method="post">
	      <input type="hidden" name="id" value="${itemDate.item_id }">
	      <input type="hidden" name="name" value="${itemDate.item_name }">
	      <input type="hidden" name="price" value="${itemDate.item_price }">
	  <label>個数 <input id="count-input" type="number" min="1" max="10" value="1" name="item_count" required></label>
	      <input class="submit" type="submit" value="カートに入れる">
	    </form>
	  </div>
  </c:if>

<div id="reviews">
	<h2>REVIEWS</h2>
	<c:if test="${err != null}">
		<p id="err-message">${err }</p>
	</c:if>

	<c:forEach var="review" items="${reviewList }">
		<div class="review-box">
			<div class="user-date">
				<img src="img/human.png">
				<p>${review.user_name }</p>
			</div>

	 		<div class="user-review">
				<h3>${review.reviews_title }</h3>
				<p>${review.reviews_text }</p>
			</div>
		</div>
	</c:forEach>
</div>

</main>

</body>
</html>
