<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- 管理者ログインページ -->

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>LOGIN</title>
<link rel="stylesheet" href="admin_css/common.css">
<link rel="stylesheet" href="admin_css/admin_login.css">
</head>
<body>

<main>
  <!-- フォーム -->
  <div id="login_content">
      <h2>LOGIN</h2>

	  <c:if test="${errMsg != null}">
	    <div class="err-text">${errMsg}</div>
	  </c:if>

      <form action="AdminLoginServlet" method="post">
        <input id="user_id" type="text" name="login_id" placeholder="login_id">
        <input id="password" type="password" name="password" placeholder="password">

        <input id="submit" type="submit">
      </form>
  </div>

</main>

<div id="admin_login"><a href="EcLoginServlet">ユーザーログイン</a></div>


</body>
</html>
