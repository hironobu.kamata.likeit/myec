<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 購入履歴　詳細 -->

<!DOCTYPE html>
<html lang="ja">
  <head>
  <meta charset="utf-8">
  <title>comfirm cart</title>
  <link rel="stylesheet" href="css/common.css">
  <link rel="stylesheet" href="css/user_detail.css">
  </head>
<body>

<header>
  <div id="header_content">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>

    <ul id="list">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </ul>
  </div>
</header>

<main>

    <h2>購入履歴詳細</h2>

    <div id="user_detail_tb">
        <table id="left">
          <tr>
            <th>購入日時</th>
            <th>合計金額</th>
          </tr>

          <tr>
            <td>${historyDate.formatDate }</td>
            <td>${historyDate.price_total } 円</td>
          </tr>

        </table>

        <table id="right">

        	  <tr>
	            <th>商品名</th>
	            <th>金額</th>
	            <th>個数</th>
	          </tr>

	        <c:forEach var="history" items="${historyDetailDate}">
	          <tr>
	            <td>${history.item_name }</td>
	            <td>${history.item_price } 円</td>
	            <td>${history.item_count }</td>
	          </tr>
			</c:forEach>
        </table>
    </div>

</main>

    <a id="back" href="UserServlet">戻る</a>



</body>
</html>
