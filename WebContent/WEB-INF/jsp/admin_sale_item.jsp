<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>addSaleItem</title>
<link rel="stylesheet" href="admin_css/common.css">
<link rel="stylesheet" href="admin_css/admin_sale_item.css">

</head>
<body>

<main>

  <div id="add_product">

    <h2>セール商品管理</h2>

	<div id="add-sale-item">

		<h3>商品を追加</h3>

	    <form action="AdminSaleServlet" method="post">
		  商品ID<br><input type="text" name="item_id"><br>
	      商品名<br><input type="text" name="item_name"><br>

	      <input id="subm" type="submit" value="追加">
	    </form>

    </div>

  </div>

  <div id="delete_product">
  	<h3>セール中のアイテム・管理</h3>

  	  <table id="sale_table">
	      <tr>
	        <th>商品ID</th>
	        <th>商品名</th>
	        <th>単価</th>
	        <th></th>
	      </tr>

		<c:forEach var="item" items="${itemList}">
	      <tr>
	        <td>商品ID: ${item.item_id } </td>
	        <td>商品名: ${item.item_name } </td>
	        <td>価格:  ${item.item_price } 円</td>
	        <td>
	          <form id="count" action="AdminSaleDeleteServlet" method="post">
	          	<input type="hidden" name="item_id" value="${item.item_id }">
	            <input type="checkbox" name="item_check">
	            <input type="submit" name="delete" value="削除">
	          </form>
	        </td>
	      </tr>
		</c:forEach>
    </table>

  </div>


   <form id="back_button" action="AdminTopServlet">
	 <input type="submit" name="back" value="戻る">
   </form>


</body>
</html>