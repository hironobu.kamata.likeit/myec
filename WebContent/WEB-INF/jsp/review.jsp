<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="charset=UTF-8">
<title>REVIEWS</title>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/reviews.css">

</head>
<body>

<header>
  <div id="header_content">
  <c:if test="${userInfo != null}">
    <h1 id="EC"><a href="ProductListServlet">EC</a></h1>
  </c:if>
  <c:if test="${userInfo == null}">
    <h1 id="EC"><a href="IndexServlet">EC</a></h1>
  </c:if>


    <ul id="list">
    <c:if test="${userInfo != null}">
      <li><a href="ComfirmCartServlet">CART</a></li>
      <li><a href="UserServlet">USER</a></li>
      <li><a href="ContactServlet">CONTACT</a></li>
      <li><a href="LogoutServlet">LOGOUT</a></li>
    </c:if>
    <c:if test="${userInfo == null}">
      <li><a href="EcLoginServlet">LOGIN</a></li>
      <li><a href="NewUserServlet">SIGN UP</a></li>
    </c:if>
    </ul>
  </div>
</header>

<main>

	<h2>レビューを書く</h2>

	<form id="review-box" action="NewReviewServlet" method="post">
		<input id="review-title" class="review-form" size="50" name="review-title" type="text" placeholder="タイトル">
		<textarea id="review-text"  cols="49" rows="10" wrap="hard" name="review-text" placeholder="本文"></textarea>
		<input type="hidden" name="item_id" value="${itemId }">
		<input type="hidden" name="user_name" value="${userName }">
		<input type="hidden" name="login_id" value="${userInfo.login_id }">

		<input id="sbm" type="submit">
	</form>

</main>

</body>
</html>










