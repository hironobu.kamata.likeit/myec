<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>delete product</title>
<link rel="stylesheet" href="admin_css/common.css">
<link rel="stylesheet" href="admin_css/delete_product.css">
</head>
<body>

<main>

  <div id="delete_box">
    <h2>商品情報削除</h2>

    <div id="user_id">
      <p>商品ID : ${itemDate.item_id }</p>
      <p>商品名 : ${itemDate.item_name }</p>
      <p>金額 : ${itemDate.item_price }</p>
      <div id="image">
        <img src="img/${itemDate.file_name }">
      </div>
    </div>

    <div id="form">

      <form id="right" action="AdminDeleteProduct" method="post">
      	<input type="hidden" name="id" value="${itemDate.item_id}">
        <input class="padd" type="submit" name="delete" value="削除">
      </form>

      <form id="left" action="AdminTopServlet">
        <input class="padd" type="submit" name="back" value="戻る">
      </form>

    </div>

  </div>


</main>

</body>
</html>
