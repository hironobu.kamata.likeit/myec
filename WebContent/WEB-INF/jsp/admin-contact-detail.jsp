<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>問合せ内容</title>
<link rel="stylesheet" href="admin_css/common.css">
<link rel="stylesheet" href="admin_css/admin-contact-detail.css">
</head>
<body>

<main>
  <h2>問合せ内容確認</h2>

  <div id="user-date">
    <p>${con.post_day }</p>
    <p>お名前 : ${con.user_name } 様</p>
    <p>内容 : ${con.contact_category }</p>
    <p>連絡先 : ${con.user_address }</p>
  </div>

  <h3>お問い合わせ内容</h3>
  <div id="text">
      <p>${con.contact_text }</p>
  </div>

  <div id="delete">
  	<form action="AdminContactDetailServlet" method="post">
  		<input type="hidden" name="id" value="${con.contact_id }">
  		<button type="submit">削除</button>
  	</form>
  </div>

  <a id="back" href="AdminContactServlet">戻る</a>

</main>




</body>
</html>